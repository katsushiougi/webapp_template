connect = require "connect"
http    = require "http"

# package.json を読み込んでおく
pkg = require "./package.json"

# requirejsのヘルパー
requirejsHelper = require "requirejs-helper"

###
Jade用のオブジェクトマージ関数
###
# getData = (options...) ->
#   unless options then return null
#   for option, i in options
#     if i == 0
#       r = _.clone option, true
#     else
#       r = _.extend r, option
#   return r

###
Jadeの設定データ
###
# jadeBaseConf = require "./src/jade/config.json"
# jadeTohokuConf = require "./src/jade/config_tohoku.json"

###
grunt設定
###
module.exports = (grunt) ->
  #
  # Gruntモジュールを読み込む
  #
  for taskName of pkg.devDependencies
    if taskName.substring(0, 6) == "grunt-" then grunt.loadNpmTasks taskName
  #
  # grunt.initConfig
  #
  config =
    pkg: grunt.file.readJSON('package.json')
    #
    # clean
    #
    clean: [
        "temp"
        "dist"
      ]
    #
    # grunt-contrib-concat
    #
    concat:
      vendors:
        files:
          "<%= pkg.path.dist %>/common/js/libs/vendor.js": [
            "libs/vendors/underscore-min.js"
            "libs/vendors/backbone-min.js"
            "libs/vendors/TweenMax.min.js"
          ]
    copy:
      libs:
        files: [
          "dist/common/js/libs/almond.min.js":"libs/almond.min.js"
          "dist/common/js/libs/modernizr.min.js":"libs/modernizr.min.js"
          "dist/common/js/libs/jquery-1.10.2.min.js":"libs/jquery-1.10.2.min.js"
        ]
      media:
        files:[
          {expand:true, src:["**/*.{gif,jpeg,jpg,png,mp3,mp4,ogg,wav,otf,ttf,swf}"], cwd:"media", dest:"dist", filter:"isFile"}
        ]
    #
    # grunt-tasks-coffee
    #
    coffee:
      dist:
        options:
          srcDir : "<%= pkg.path.src %>/coffee"
          destDir: "temp/js"
          bare   : false
    #
    # grunt-tasks-stylus
    #
    stylus:
      dist:
        options:
          srcDir  : "<%= pkg.path.src %>/styl"
          destDir : "<%= pkg.path.dist %>/common/css"
          # 外部から変数を定義するときは data に書く
          # data    : SIZE: 15
    #
    # grunt-tasks-jade
    #
    jade:
      dist:
        options:
          # ファイル名: 新しくファイルを追加するときはここに名前を追加する
          files:[
            "index"
          ]
          srcDir  : "<%= pkg.path.src %>/jade"
          destDir : "<%= pkg.path.dist %>"
          # 外部から変数を定義するときは data に書く
          data: (dest, src) ->
            return require('./src/jade/config.json')
            # if dest.indexOf("tohoku") >= 0
            #   return getData( jadeBaseConf, jadeTohokuConf )
            # else
            #   return jadeBaseConf
    #
    # grunt-contrib-imagemin
    #
    imagemin:
      options:
        # pngを8bitに変換する
        pngquant: false
      source:
        expand: true
        cwd   : "<%= pkg.path.dist %>/common/img"
        src   : "**/*.{png,jpg}"
        dest  :"<%= pkg.path.dist %>/common/img"
    #
    # grunt-glue
    # https://bitbucket.org/carkraus/grunt-glue
    # http://glue.readthedocs.org/en/latest/quickstart.html
    # cssはstylus用にリネームタスクを追加で実行する
    #
    glue:
      sprites:
        src : "<%= pkg.path.src %>/sprites"
        options: '--css=src/styl --img=dist/common/img/sprites --margin=10 --namespace=sp --optipng'
    #
    # grunt-rename
    #
    rename:
      sprites:
        files: [
          {src: ["<%= pkg.path.src %>/styl/sprites.css"], dest: "<%= pkg.path.src %>/styl/sprites.styl"}
        ]

  #
  # grunt.initConfig
  #
  grunt.initConfig( config )

  #
  # リネームする
  #
  grunt.renameTask "stylus-watch", "stylus"
  grunt.renameTask "jade-watch", "jade"
  grunt.renameTask "coffee-watch", "coffee"

  #
  # ======== タスクの登録
  #

  #
  #デフォルトタスク - 何もしない
  #
  grunt.registerTask( "default", [] )
  #
  #開発用タスク実行
  #
  grunt.registerTask "start", [
    # "sprites"
    "concat"
    "copy"
    "jade::watch"
    "stylus::watch"
    "coffee::watch"
    "requirejs"
    "connect"
  ]
  ###
  リリース用タスク実行
  ###
  grunt.registerTask "build", [
    # "sprites"
    "clean"
    "concat"
    "copy"
    "jade::compact"
    "stylus::compact"
    "coffee::compact"
    "requirejs:release"
    # "imagemin"
  ]
  #
  # スプライト用タスク
  #
  grunt.registerTask "sprites", [ "glue", "rename"]
  #
  # requirejsHelper.config
  #
  requirejsHelper.config
    dist:
      inDir  : "temp/js"
      outDir : pkg.path.dist + "/common/js"
      names  : [
        # 新しくビルド対象を追加するときはここに追加する
        "main"
      ]
  #
  # require.js タスク作成
  #   grunt requirejs
  #   grunt requirejs:release
  #
  grunt.registerTask "requirejs", "requirejs(:release)", (release)->
    done = @async()
    requirejsHelper.build release == "release", ()->
      console.info "Complete"
      done()

  #
  # connect (web server) を起動
  #
  grunt.registerTask "connect", "start connet server",()->
    APP_ROOT = pkg.path.dist
    # connect のインスタンス生成
    connect = require("connect")
    app     = connect()
    # requirejs のミドルウェア設定
    app.use( requirejsHelper.middleware( APP_ROOT ) )
    # アプリ関連ファイル用
    app.use( connect.static( APP_ROOT ) )
    # 開始
    app.listen( 3000 )
    #
    @async()